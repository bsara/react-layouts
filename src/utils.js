/* eslint-disable import/prefer-default-export */
/** !
 * @bsara/react-layouts v1.3.4
 *
 * Copyright (c) 2019, Brandon D. Sara (https://bsara.pro/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/react-each/blob/master/LICENSE)
 */

const GLOBAL_HTML_ATTRIBUTE_NAMES = [
  'accesskey',
  'autocapitalize',
  'className',
  'contenteditable',
  'contextmenu',
  'dir',
  'draggable',
  'dropzone',
  'hidden',
  'id',
  'is',
  'itemid',
  'itemprop',
  'itemref',
  'itemscope',
  'itemtype',
  'lang',
  'slot',
  'spellcheck',
  'style',
  'tabindex',
  'title',
  'translate'
];

const EVENT_HANDLER_REGEX = /^on[A-Z]/;



export function pickGlobalHtmlAttributeProps(obj) {
  const newObj = {};

  for (let propName in obj) {
    if (GLOBAL_HTML_ATTRIBUTE_NAMES.includes(propName) || propName.startsWith('data-') || propName.match(EVENT_HANDLER_REGEX)) {
      newObj[propName] = obj[propName];
    }
  }

  return newObj;
}
