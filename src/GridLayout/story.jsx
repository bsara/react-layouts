/**
 * ISC License (ISC)
 *
 * Copyright (c) 2019, Brandon D. Sara (https://bsara.pro/)
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

import React from 'react';
import { storiesOf } from '@storybook/react';

import GridLayout from '.';

import './story.css';



const contrastStyles = {
  backgroundColor: '#eee',
  border:          '1px solid lightgray'
};


function _StoryWrapper({ lines, children }) {
  return (
    <div>
      <div>
        <a href={`https://gitlab.com/bsara/react-layouts/blob/master/src/GridLayout/story.jsx#L${lines}`}>View story source</a><br />
      </div>
      <br />
      {children}
    </div>
  );
}

class _StylesCode extends React.Component {

  constructor(...args) {
    super(...args);

    this.state = {
      visible: false
    };

    this._onToggleVisibility = this._onToggleVisibility.bind(this);
  }


  render() {
    return (
      <React.Fragment>
        <div>
          <a href="javascript:void(0)" onClick={this._onToggleVisibility}>{this.state.visible ? "Hide" : "Show"} Applicable CSS</a>
        </div>
        {this.state.visible && (
          <div style={{ background: '#ddd', padding: '10px', marginTop: '2px' }}>
            <code style={{ font: 'monospace', whiteSpace: 'pre' }}>
              {this.props.children.trim()}
            </code>
          </div>
        )}
        <br />
      </React.Fragment>
    );
  }


  _onToggleVisibility() {
    this.setState({ visible: !this.state.visible });
  }
}



storiesOf('GridLayout', module)
  .add('Default', () => (
    <_StoryWrapper lines="88-108">
      <GridLayout style={contrastStyles}>
        <div>Item 1.1</div>
        <div>Item 1.2</div>
        <div>Item 1.3</div>
        <div>Item 1.4</div>

        <div>Item 2.1</div>
        <div>Item 2.2</div>
        <div>Item 2.3</div>
        <div>Item 2.4</div>

        <div>Item 3.1</div>
        <div>Item 3.2</div>
        <div>Item 3.3</div>
        <div>Item 3.4</div>

        <div>Item 4.1</div>
        <div>Item 4.2</div>
        <div>Item 4.3</div>
        <div>Item 4.4</div>
      </GridLayout>
    </_StoryWrapper>
  ))
  .add('README Example', () => (
    <_StoryWrapper lines="127-132">
      <_StylesCode>
        {`
.my-grid-layout {
  --grid-column-count: 3;
}

.my-cell0-0 {
  --grid-row-span: 2;
}

.my-cell0-1 {
  --grid-column-span: 2;
}`}
      </_StylesCode>
      <GridLayout styleName="_g-readme-layout">
        <span styleName="_g-readme-cell0-0">Item 0.0</span>
        <span styleName="_g-readme-cell0-1">Item 0.1</span>
        <span>Item 1.1</span>
        <span>Item 1.2</span>
      </GridLayout>
    </_StoryWrapper>
  ))
  .add('Column Count (4)', () => (
    <_StoryWrapper lines="144-164">
      <_StylesCode>
        {`
.my-grid-layout {
  --grid-column-count: 4;
}
        `}
      </_StylesCode>
      <GridLayout styleName="_g-column-count" style={contrastStyles}>
        <div>Item 1.1</div>
        <div>Item 1.2</div>
        <div>Item 1.3</div>
        <div>Item 1.4</div>

        <div>Item 2.1</div>
        <div>Item 2.2</div>
        <div>Item 2.3</div>
        <div>Item 2.4</div>

        <div>Item 3.1</div>
        <div>Item 3.2</div>
        <div>Item 3.3</div>
        <div>Item 3.4</div>

        <div>Item 4.1</div>
        <div>Item 4.2</div>
        <div>Item 4.3</div>
        <div>Item 4.4</div>
      </GridLayout>
    </_StoryWrapper>
  ))
  .add('Column Count (4 w/Gap)', () => (
    <_StoryWrapper lines="177-197">
      <_StylesCode>
        {`
.my-grid-layout {
  --grid-column-count: 4;
  grid-gap: 10px;
}
        `}
      </_StylesCode>
      <GridLayout styleName="_g-column-count _g-gap" style={contrastStyles}>
        <div>Item 1.1</div>
        <div>Item 1.2</div>
        <div>Item 1.3</div>
        <div>Item 1.4</div>

        <div>Item 2.1</div>
        <div>Item 2.2</div>
        <div>Item 2.3</div>
        <div>Item 2.4</div>

        <div>Item 3.1</div>
        <div>Item 3.2</div>
        <div>Item 3.3</div>
        <div>Item 3.4</div>

        <div>Item 4.1</div>
        <div>Item 4.2</div>
        <div>Item 4.3</div>
        <div>Item 4.4</div>
      </GridLayout>
    </_StoryWrapper>
  ))
  .add('Row Count (4)', () => (
    <_StoryWrapper lines="209-229">
      <_StylesCode>
        {`
.my-grid-layout {
  --grid-row-count: 4;
}
        `}
      </_StylesCode>
      <GridLayout styleName="_g-row-count" style={contrastStyles}>
        <div>Item 1.1</div>
        <div>Item 1.2</div>
        <div>Item 1.3</div>
        <div>Item 1.4</div>

        <div>Item 2.1</div>
        <div>Item 2.2</div>
        <div>Item 2.3</div>
        <div>Item 2.4</div>

        <div>Item 3.1</div>
        <div>Item 3.2</div>
        <div>Item 3.3</div>
        <div>Item 3.4</div>

        <div>Item 4.1</div>
        <div>Item 4.2</div>
        <div>Item 4.3</div>
        <div>Item 4.4</div>
      </GridLayout>
    </_StoryWrapper>
  ))
  .add('Row Count (4 w/Gap)', () => (
    <_StoryWrapper lines="242-262">
      <_StylesCode>
        {`
.my-grid-layout {
  --grid-row-count: 4;
  grid-gap: 10px;
}
        `}
      </_StylesCode>
      <GridLayout styleName="_g-row-count _g-gap" style={contrastStyles}>
        <div>Item 1.1</div>
        <div>Item 1.2</div>
        <div>Item 1.3</div>
        <div>Item 1.4</div>

        <div>Item 2.1</div>
        <div>Item 2.2</div>
        <div>Item 2.3</div>
        <div>Item 2.4</div>

        <div>Item 3.1</div>
        <div>Item 3.2</div>
        <div>Item 3.3</div>
        <div>Item 3.4</div>

        <div>Item 4.1</div>
        <div>Item 4.2</div>
        <div>Item 4.3</div>
        <div>Item 4.4</div>
      </GridLayout>
    </_StoryWrapper>
  ))
  .add('Areas Template', () => (
    <_StoryWrapper lines="297-317">
      <_StylesCode>
        {`
.my-grid-layout {
  grid-template-areas: "three-one   one-one   four-one   two-one"
                       "three-two   one-two   four-two   two-two"
                       "three-three one-three four-three two-three"
                       "three-four  one-four  four-four  two-four";
}

.cell-1-1 { grid-area: one-one; }
.cell-1-2 { grid-area: one-two; }
.cell-1-3 { grid-area: one-three; }
.cell-1-4 { grid-area: one-four; }

.cell-2-1 { grid-area: two-one; }
.cell-2-2 { grid-area: two-two; }
.cell-2-3 { grid-area: two-three; }
.cell-2-4 { grid-area: two-four; }

.cell-3-1 { grid-area: three-one; }
.cell-3-2 { grid-area: three-two; }
.cell-3-3 { grid-area: three-three; }
.cell-3-4 { grid-area: three-four; }

.cell-4-1 { grid-area: four-one; }
.cell-4-2 { grid-area: four-two; }
.cell-4-3 { grid-area: four-three; }
.cell-4-4 { grid-area: four-four; }
        `}
      </_StylesCode>
      <GridLayout styleName="_g-areas-template" style={contrastStyles}>
        <div>Item 1.1</div>
        <div>Item 1.2</div>
        <div>Item 1.3</div>
        <div>Item 1.4</div>

        <div>Item 2.1</div>
        <div>Item 2.2</div>
        <div>Item 2.3</div>
        <div>Item 2.4</div>

        <div>Item 3.1</div>
        <div>Item 3.2</div>
        <div>Item 3.3</div>
        <div>Item 3.4</div>

        <div>Item 4.1</div>
        <div>Item 4.2</div>
        <div>Item 4.3</div>
        <div>Item 4.4</div>
      </GridLayout>
    </_StoryWrapper>
  ))
  .add('Columns Template', () => (
    <_StoryWrapper lines="329-349">
      <_StylesCode>
        {`
.my-grid-layout {
  grid-template-columns: minmax(max-content, 50fr) minmax(min-content, 75fr) minmax(min-content, 35fr) minmax(min-content, 10fr);
}
        `}
      </_StylesCode>
      <GridLayout styleName="_g-columns-template" style={contrastStyles}>
        <div>Item 1.1</div>
        <div>Item 1.2 blahblahblah</div>
        <div>Item 1.3</div>
        <div>Item 1.4</div>

        <div>Item 2.1</div>
        <div>Item 2.2</div>
        <div>Item 2.3</div>
        <div>Item 2.4</div>

        <div>Item 3.1</div>
        <div>Item 3.2</div>
        <div>Item 3.3</div>
        <div>Item 3.4</div>

        <div>Item 4.1</div>
        <div>Item 4.2</div>
        <div>Item 4.3</div>
        <div>Item 4.4</div>
      </GridLayout>
    </_StoryWrapper>
  ))
  .add('Rows Template', () => (
    <_StoryWrapper lines="361-381">
      <_StylesCode>
        {`
.my-grid-layout {
  grid-template-rows: repeat(4, 50px);
}
        `}
      </_StylesCode>
      <GridLayout styleName="_g-rows-template" style={contrastStyles}>
        <div>Item 1.1</div>
        <div>Item 1.2</div>
        <div>Item 1.3</div>
        <div>Item 1.4</div>

        <div>Item 2.1</div>
        <div>Item 2.2</div>
        <div>Item 2.3</div>
        <div>Item 2.4</div>

        <div>Item 3.1</div>
        <div>Item 3.2</div>
        <div>Item 3.3</div>
        <div>Item 3.4</div>

        <div>Item 4.1</div>
        <div>Item 4.2</div>
        <div>Item 4.3</div>
        <div>Item 4.4</div>
      </GridLayout>
    </_StoryWrapper>
  ))
  .add('Item Size (Column Count)', () => (
    <_StoryWrapper lines="394-414">
      <_StylesCode>
        {`
.my-grid-layout {
  --grid-item-size: 100px;
  --grid-column-count: 4;
}
        `}
      </_StylesCode>
      <GridLayout styleName="_g-item-size _g-column-count" style={contrastStyles}>
        <div>Item 1.1</div>
        <div>Item 1.2</div>
        <div>Item 1.3</div>
        <div>Item 1.4</div>

        <div>Item 2.1</div>
        <div>Item 2.2</div>
        <div>Item 2.3</div>
        <div>Item 2.4</div>

        <div>Item 3.1</div>
        <div>Item 3.2</div>
        <div>Item 3.3</div>
        <div>Item 3.4</div>

        <div>Item 4.1</div>
        <div>Item 4.2</div>
        <div>Item 4.3</div>
        <div>Item 4.4</div>
      </GridLayout>
    </_StoryWrapper>
  ))
  .add('Item Size (Row Count)', () => (
    <_StoryWrapper lines="427-447">
      <_StylesCode>
        {`
.my-grid-layout {
  --grid-item-size: 100px;
  --grid-row-count: 4;
}
        `}
      </_StylesCode>
      <GridLayout styleName="_g-item-size _g-row-count" style={contrastStyles}>
        <div>Item 1.1</div>
        <div>Item 1.2</div>
        <div>Item 1.3</div>
        <div>Item 1.4</div>

        <div>Item 2.1</div>
        <div>Item 2.2</div>
        <div>Item 2.3</div>
        <div>Item 2.4</div>

        <div>Item 3.1</div>
        <div>Item 3.2</div>
        <div>Item 3.3</div>
        <div>Item 3.4</div>

        <div>Item 4.1</div>
        <div>Item 4.2</div>
        <div>Item 4.3</div>
        <div>Item 4.4</div>
      </GridLayout>
    </_StoryWrapper>
  ));
