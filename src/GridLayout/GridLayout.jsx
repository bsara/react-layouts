/** !
 * @bsara/react-layouts v1.3.4
 *
 * Copyright (c) 2019, Brandon D. Sara (https://bsara.pro/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/react-each/blob/master/LICENSE)
 */

import React from 'react';
import PropTypes from 'prop-types';

import { pickGlobalHtmlAttributeProps } from '../utils';

import './GridLayout.css';



function _GridLayout(props, ref) {
  let styleNames = 'grid-layout';

  if (props.inline) {
    styleNames += ' grid-layout-inline';
  }

  return (
    <div {...pickGlobalHtmlAttributeProps(props)} ref={ref || props.domRef} className={props.className} styleName={styleNames}>
      {props.children}
    </div>
  );
}


const GridLayout = React.forwardRef(_GridLayout);

GridLayout.propTypes = {
  id:        PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  className: PropTypes.string,
  style:     PropTypes.object,

  inline: PropTypes.bool
};

export default GridLayout;
