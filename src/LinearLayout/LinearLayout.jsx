/** !
 * @bsara/react-layouts v1.3.4
 *
 * Copyright (c) 2019, Brandon D. Sara (https://bsara.pro/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/react-each/blob/master/LICENSE)
 */

import React from 'react';
import PropTypes from 'prop-types';

import { pickGlobalHtmlAttributeProps } from '../utils';

import './LinearLayout.css';



function _LinearLayout(props, ref) {
  const isDirectionVertical = (props.direction === 'v' || props.direction === 'vert' || props.direction === 'vertical');

  const directionalStyleName = (isDirectionVertical ? 'linear-layout-vertical' : 'linear-layout-horizontal');


  let styleNames = `linear-layout ${directionalStyleName}`;

  if (!props.omitItemGap) {
    styleNames += ` ${directionalStyleName}-with-item-gap`;
  }

  if (props.inline) {
    styleNames += ' linear-layout-inline';
  }

  if (props.wrap) {
    styleNames += ' linear-layout-wrapped';
  }


  return (
    <div {...pickGlobalHtmlAttributeProps(props)} ref={ref || props.domRef} className={props.className} styleName={styleNames}>
      {props.children}
    </div>
  );
}


const LinearLayout = React.forwardRef(_LinearLayout);

LinearLayout.propTypes = {
  id:        PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  className: PropTypes.string,
  style:     PropTypes.object,

  direction:   PropTypes.oneOf(['h', 'horiz', 'horizontal', 'v', 'vert', 'vertical']),
  inline:      PropTypes.bool,
  wrap:        PropTypes.bool,
  omitItemGap: PropTypes.bool
};

export default LinearLayout;
