/** !
 * @bsara/react-layouts v1.3.4
 *
 * Copyright (c) 2019, Brandon D. Sara (https://bsara.pro/)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/react-each/blob/master/LICENSE)
 */

import React from 'react';
import PropTypes from 'prop-types';

import { pickGlobalHtmlAttributeProps } from '../utils';

import LinearLayout from '../LinearLayout';

import './ResponsiveLinearLayout.css';



function _ResponsiveLinearLayout(props, ref) {
  return (
    <LinearLayout {...pickGlobalHtmlAttributeProps(props)}
                  ref={ref || props.domRef}
                  direction={props.direction}
                  inline={props.inline}
                  omitItemGap
                  className={props.className}
                  styleName="responsive-linear-layout">
      {props.children}
    </LinearLayout>
  );
}


const ResponsiveLinearLayout = React.forwardRef(_ResponsiveLinearLayout);

ResponsiveLinearLayout.propTypes = {
  id:        PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]),
  className: PropTypes.string,
  style:     PropTypes.object,

  direction: PropTypes.oneOf([ 'h', 'horiz', 'horizontal', 'v', 'vert', 'vertical' ]),
  inline:    PropTypes.bool,
  wrap:      PropTypes.bool
};

export default ResponsiveLinearLayout;
