/* eslint-env node */
/* eslint-disable global-require */


module.exports = {
  map: { inline: false },

  plugins: [
    require('postcss-nesting'),
    require('autoprefixer'),
    require('postcss-discard-comments')
  ]
};
