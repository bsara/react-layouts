# @bsara/react-layouts [![NPM Package](https://img.shields.io/npm/v/@bsara/react-layouts.svg?style=flat-square)][npm]

[![ISC License](https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square)][license]


[Storybook](https://bsara.gitlab.io/react-layouts)

[Changelog](https://gitlab.com/bsara/react-layouts/blob/master/CHANGELOG.md)


> A collection of generic, reusable layout components for React.



# Install

**Project Install**
```bash
$ npm install --save @bsara/react-layouts
```



# API

## Table of Contents

- [LinearLayout](#linearlayout)
- [ResponsiveLinearLayout](#responsivelinearlayout)
- [GridLayout](#gridlayout)


<br/>


## LinearLayout

> A layout that arranges children either horizontally in a single column or vertically in
> a single row.

At it's core, this component is basically a [CSS flexbox](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox).
As such, all CSS flexbox properties are valid when styling a `LinearLayout`. This also
means that this component is NOT supported in browsers that do not support CSS flexbox
without appropriate polyfills.

Some sensible CSS defaults have been set and some CSS custom properties have been provided
as a convenience *(see the documented CSS custom properties below for more details)*.


### Props

> Any prop that is acceptable by a `div` component is acceptable by a `LinearLayout`
> component.

- **direction** `?String` - *Default = `"horizontal"`*

  The direction to arrange the component's children.

  Possible values:
    - `"horizontal"`
    - `"horiz"`
    - `"h"`
    - `"vertical"`
    - `"vert`
    - `"v"`

- **inline** `?Boolean` - *Default = `false`*

  Determines whether or not the layout should be treated as an inline element or a block
  element.

- **wrap** `?Boolean` - *Default = `false`*

  When `true`, list will be allowed to wrap instead of overflow.

- **ref** `?Function`

  [Forwarded ref][forwarding-refs] to underlying DOM element.


### CSS Custom Properties

- **--linear-layout-item-gap** `Same types as any "margin" CSS property`

  Sets the gap (I.E. margin) between all direct children of a `LinearLayout`.


### Immediate Children CSS Custom Properties

- **--sub-linear-layout-item-gap** `Same types as any "margin" CSS property`

  Sets the gap (I.E. margin) between all direct children of a `LinearLayout`.

  > **NOTE:** `--linear-layout-item-gap` takes precedence over this custom property, but
  > it is provided for situations where you may need to nest a `LinearLayout` as a direct
  > child of another `LinearLayout`.

- **--linear-layout-item-size** `Same types as "width" or "height" CSS properties`

  Sets the height *(if `direction` = `"vertical"`)* or width *(if `direction` =
  `"horizontal"`)* of all direct children of a `LinearLayout`.

- **--linear-layout-item-min-size** `Same types as "min-width" or "min-height" CSS properties`

  Sets the minimum height *(if `direction` = `"vertical"`)* or width *(if `direction` =
  `"horizontal"`)* of all direct children of a `LinearLayout`.

- **--linear-layout-item-max-size** `Same types as "max-width" or "max-height" CSS properties`

  Sets the maximum height *(if `direction` = `"vertical"`)* or width *(if `direction` =
  `"horizontal"`)* of all direct children of a `LinearLayout`.



### Examples

[Storybook Examples](https://bsara.gitlab.io/react-layouts?selectedKind=LinearLayout&selectedStory=Default)

**MyLinearLayoutComponent.jsx**

```jsx
import React from 'react';
import LinearLayout from '@bsara/react-layouts/LinearLayout';
import './MyLinearLayoutComponent.css';


export default function MyLinearLayoutComponent(props) {
  return (
    <LinearLayout {...props} className="layout" direction="horizontal">
      <a>Anchor 0</a>
      <a>Anchor 1</a>
      <a>Anchor 2</a>
      <LinearLayout className="sub-layout" direction="vertical">
        <a>Sub Anchor 0</a>
        <a>Sub Anchor 1</a>
        <a>Sub Anchor 2</a>
      </LinearLayout>
    </LinearLayout>
  );
}
```


**MyLinearLayoutComponent.css**

```css
.layout {
  --linear-layout-item-gap: 5px;
}

.sub-layout > * {
  --linear-layout-item-gap: 10px;
}
```


**Output (Text Representation)**

```txt
The component above will lay out it's children similar to the following:

    |----------|----------|----------|----------------|
    | Anchor 0 | Anchor 1 | Anchor 2 ||--------------||
    |          |          |          || Sub Anchor 0 ||
    |          |          |          ||--------------||
    |          |          |          || Sub Anchor 1 ||
    |          |          |          ||--------------||
    |          |          |          || Sub Anchor 2 ||
    |          |          |          ||--------------||
    |----------|----------|----------|----------------|
```



<br/>



## ResponsiveLinearLayout

> A layout that arranges children either horizontally in a single column or vertically in
> a single row and is responsive to size changes.

This component is exactly the same a [`LinearLayout`](#linearlayout) except that it has
built in responsive styles when the CSS custom prop `--responsive-linear-layout-item-gap`
is used. See the docs for [`LinearLayout`](#linearlayout) for more information.


### Props

> Any prop that is acceptable by a `div` component is acceptable by a
> `ResponsiveLinearLayout` component.

- **direction** `?String` - *Default = `"horizontal"`*

  The direction to arrange the component's children.

  Possible values:
    - `"horizontal"`
    - `"horiz"`
    - `"h"`
    - `"vertical"`
    - `"vert`
    - `"v"`

- **inline** `?Boolean` - *Default = `false`*

  Determines whether or not the layout should be treated as an inline element or a block
  element.

- **wrap** `?Boolean` - *Default = `false`*

  When `true`, list will be allowed to wrap instead of overflow.

- **ref** `?Function`

  [Forwarded ref][forwarding-refs] to underlying DOM element.


### CSS Custom Properties

- **--responsive-linear-layout-item-gap** `Same types as any "margin" CSS property`

  Sets the gap (I.E. margin) between all direct children of a `ResponsiveLinearLayout`.


### Immediate Children CSS Custom Properties

- **--linear-layout-item-size** `Same types as "width" or "height" CSS properties`

  Sets the height *(if `direction` = `"vertical"`)* or width *(if `direction` =
  `"horizontal"`)* of all direct children of a `ResponsiveLinearLayout`.

- **--linear-layout-item-min-size** `Same types as "min-width" or "min-height" CSS properties`

  Sets the minimum height *(if `direction` = `"vertical"`)* or width *(if `direction` =
  `"horizontal"`)* of all direct children of a `ResponsiveLinearLayout`.

- **--linear-layout-item-max-size** `Same types as "max-width" or "max-height" CSS properties`

  Sets the maximum height *(if `direction` = `"vertical"`)* or width *(if `direction` =
  `"horizontal"`)* of all direct children of a `ResponsiveLinearLayout`.



### Examples

[Storybook Examples](https://bsara.gitlab.io/react-layouts?selectedKind=ResponsiveLinearLayout&selectedStory=Default)

**MyResponsiveLinearLayoutComponent.jsx**

```jsx
import React from 'react';
import LinearLayout from '@bsara/react-layouts/LinearLayout';
import ResponsiveLinearLayout from '@bsara/react-layouts/ResponsiveLinearLayout';
import './MyResponsiveLinearLayoutComponent.css';


export default function MyResponsiveLinearLayoutComponent(props) {
  return (
    <ResponsiveLinearLayout {...props} className="layout" direction="horizontal">
      <a>Anchor 0</a>
      <a>Anchor 1</a>
      <a>Anchor 2</a>
      <LinearLayout className="sub-layout" direction="vertical">
        <a>Sub Anchor 0</a>
        <a>Sub Anchor 1</a>
        <a>Sub Anchor 2</a>
      </LinearLayout>
    </ResponsiveLinearLayout>
  );
}
```


**MyResponsiveLinearLayoutComponent.css**

```css
.layout {
  --responsive-linear-layout-item-gap: 5px;
}

.sub-layout > * {
  --linear-layout-item-gap: 10px;
}
```


**Output (Text Representation)**

```txt
The component above will lay out it's children similar to the following (but will also
be responsive to sizing changes):

    |----------|----------|----------|----------------|
    | Anchor 0 | Anchor 1 | Anchor 2 ||--------------||
    |          |          |          || Sub Anchor 0 ||
    |          |          |          ||--------------||
    |          |          |          || Sub Anchor 1 ||
    |          |          |          ||--------------||
    |          |          |          || Sub Anchor 2 ||
    |          |          |          ||--------------||
    |----------|----------|----------|----------------|
```



<br/>



## GridLayout

> A layout that places its children in a rectangular grid.

This layout is basically a [CSS grid](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout).
As such, all CSS grid properties are valid when styling a `GridLayout`. This also means
that this component is NOT supported in browsers that do not support CSS grid without
appropriate polyfills.

Some sensible CSS defaults have been set and some CSS custom properties have been provided
as a convenience *(see the documented CSS custom properties below for more details)*.


### Props

> Any prop that is acceptable by a `div` component is acceptable by a `GridLayout`
> component.

- **inline** `?Boolean` - *Default = `false`*

  Determines whether or not the layout should be treated as an inline element or a block
  element.

- **ref** `?Function`

  [Forwarded ref][forwarding-refs] to underlying DOM element.


### CSS Custom Properties

- **--grid-row-count** `?Number`

  When used, automatically styles a `GridLayout` with the number of rows specified.

- **--grid-column-count** `?Number`

  When used, automatically styles a `GridLayout` with the number of columns specified.


### Immediate Children CSS Custom Properties

- **--grid-row-span** `?Number`

  Sets the number of rows a direct child of `GridLayout` should span.

- **--grid-column-span** `?Number`

  Sets the number of columns a direct child of `GridLayout` should span.


- **--grid-item-size** *`Same types as "width" or "height" CSS properties`*

  Sets the height and width of all direct children of a `GridLayout`.

- **--grid-item-min-size** *`Same types as "min-width" or "min-height" CSS properties`*

  Sets the minimum height and width of all direct children of a `GridLayout`.

- **--grid-item-max-size** *`Same types as "max-width" or "max-height" CSS properties`*

  Sets the maximum height and width of all direct children of a `GridLayout`.


- **--grid-item-width** *`Same types as "width" CSS property`*

  Sets the width of all direct children of a `GridLayout`.

- **--grid-item-min-width** *`Same types as "min-width" CSS property`*

  Sets the minimum width of all direct children of a `GridLayout`.

- **--grid-item-max-width** *`Same types as "max-width" CSS property`*

  Sets the maximum width of all direct children of a `GridLayout`.


- **--grid-item-height** *`Same types as "height" CSS property`*

  Sets the height of all direct children of a `GridLayout`.

- **--grid-item-min-height** *`Same types as "min-height" CSS property`*

  Sets the minimum height of all direct children of a `GridLayout`.

- **--grid-item-max-height** *`Same types as "max-height" CSS property`*

  Sets the maximum height of all direct children of a `GridLayout`.



### Examples

[Storybook Examples](https://bsara.gitlab.io/react-layouts?selectedKind=GridLayout&selectedStory=Default)

**MyGridLayoutComponent.jsx**

```jsx
import React from 'react';
import GridLayout from '@bsara/react-layouts/GridLayout';
import './MyGridLayoutComponent.css';


export default function MyGridLayoutComponent(props) {
  return (
    <GridLayout {...props} className="layout">

      <span className="cell0-0">Item 0.0</span>
      <span className="cell0-1">Item 0.1</span>

      <span>Item 1.1</span>
      <span>Item 1.2</span>

    </GridLayout>
  );
}
```


**MyGridLayoutComponent.css**

```css
.layout {
  --grid-column-count: 3;
}

.cell0-0 {
  --grid-row-span: 2;
}

.cell0-1 {
  --grid-column-span: 2;
}
```


**Output (Text Representation)**

```txt
The component above will lay out it's children similar to the following:

    |----------|---------------------|
    | Item 0.0 | Item 0.1            |
    |          |----------|----------|
    |          | Item 1.1 | Item 1.2 |
    |----------|----------|----------|
```


<br/>
<br/>


# License

ISC License (ISC)

Copyright (c) 2019, Brandon D. Sara (http://bsara.pro/)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.



[bsara-home]: http://bsara.pro/
[license]:    https://gitlab.com/bsara/react-layouts/blob/master/LICENSE "License"
[npm]:        https://www.npmjs.com/package/@bsara/react-layouts         "NPM Package: react-layouts"

[forwarding-refs]: https://reactjs.org/docs/forwarding-refs.html "Forwarding Refs"
