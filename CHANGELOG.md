## v1.3.3 - 2018-10-30

- **[New Feature]** Added `wrap` prop to `LinearLayout` and `ResponsiveLinearLayout`,
  which allows wrapping when the list is rendered.


## v1.3.2 - 2018-10-23

- **[Bug Fix]** Fixed `className` issues introduced in previous release where the
  `className` prop was being ignored.
- **[Bug Fix]** Fixed usage of `forwardRef` in all components. 


## v1.3.0 - 2018-10-22

- **[Deprecated]** `domRef` has been deprecated. The library now uses [forwarding refs](https://reactjs.org/docs/forwarding-refs.html)
  instead. Use `ref` from now on.


## v1.2.0 - 2018-10-09

- Reverted changes from v1.1.0 _(decided that it was too specific of a feature to add)_.


## v1.0.2 - 2018-08-08

- Updated `LinearLayout` styles so that it is easier to override item margins if needed.


## v1.0.1 - 2018-08-08

- Initial release
